<H1><IMG SRC="logos/ASF_Challenge.jpg" ALT="ASF Challenge logo">ASF Challenge 2020</H1>

<A HREF="http://creativecommons.org/licenses/by-nc-sa/4.0/">
<IMG SRC="logos/cc-by-nc-sa.png" ALT="License: CC-BY-NC-SA 4.0" HEIGHT="50px"></A>

![Version](https://img.shields.io/badge/EMULSION%20version-1.1rc5-8cd0c3.svg)


[[_TOC_]]

Related publication
-------------------

This public repository contains the model and documents used in the
[:pig: :boar: ASF Challenge](https://www6.inrae.fr/asfchallenge/) (Aug 2020-Jan
2021), coordinated by Pauline Ezanno, Sébastien Picault and Timothée
Vergne (INRAE). It is published as supplementary information for the
following article:

> "The African swine fever modelling challenge: Objectives, model description and synthetic data generation"<BR>
Sébastien Picault, Timothée Vergne, Matthieu Mancini, Servane Bareille, Pauline Ezanno (2022), *Epidemics* **40**(100616)<BR>
https://doi.org/10.1016/j.epidem.2022.100616<BR>
Part of the special issue ["The first modelling challenge in animal health: ASF spread at the interface between wild boar and domestic pigs"](https://www.sciencedirect.com/journal/epidemics/special-issue/10W50R8KX1V)



Content of the repository
-------------------------

- `README.md`: this file
- `model`: directory with model files
  - `ppa.yaml`: model file (in EMULSION modelling language)
  - `ppa.py`: additional Python file to implement specific features not provided by EMULSION (kernel diffusion, special logging, specific control measures...)
  - `outputs/challenge`: simulation outputs corresponding to the trajectory used for the challenge
  - `data`: directory containing input data used to feed the model
	- `boars.csv`: spatial distribution of the centres of home ranges of wild boar
	- `herds.csv`: spatial distribution and features of pig farms
	- `moves.csv`: pre-defined trade movements between pig farms
	- `create_tiles.sh`: script to discretize the whole island into 15x15 km tiles, create the diffusion kernels (as sparse matrices), and store them into `tiles_15000`
	- `list_of_tiles.txt`: list of non-empty 15x15 km$`^2`$ tiles
	- `extract_area.py`: Python script to (re)build 1 specific tile
	- `fences.csv`, `buffer.csv`: list of 15x15 km$`^2`$ tiles which represent the fenced area and the surrounding buffer, respectively
- `kick-of_meeting`: slides used for the kick-off meeting (August 2020)
- `Data_all_players`: initial data and situation reports provided for each challenge period




Data provided to ASF Challenge participants ("players")
-------------------------------------------------------

Directory `Data_all_players` contains the data that was given to the participants at the beginning of the Challenge (day 50 after detection of the first case) and during the subsequent periods (days 80 and 110).

- `initial_data` was provided at day 50 and contained:
  - a map of the fictitious island (administrative divisions and landcover, shapefiles: `Island_ADMIN` and `Island_LANDCOVER`)
  - a list of all officially registered pig farms (`herds.csv`) with their characteristics (this list was built by removing from the original input file in `model/data` 10% of small herds, and re-numbering the farms)
  - the wild boar hunting bags by administrative division of the island (`WB_HundintBag.csv`)
  - the list of trade movements between farms from 59 days before the detection of the first case up to day 50 (`moves.csv`)
  - a description of data structure (`DataDoc_Player.pdf`)
- `DAY_50`, `DAY_80`, `DAY_110` all have the same structure and contain reports at the specified date (after the detection of the first case):
  - `DAY<N>.pdf`, the situation report and proposed control measures or specific questions to the participants
  - `herds_day_<N>.csv`, an updated list of pig farms (possibly with unregistered pig farms discovered since last situation report)
  - `moves_Players_day_<N>.csv`, an updated list of trade movements
  - `TimeSeries_day_<N>.csv`, the updated time series representing epidemiological events (detections, cullings, etc.) and their nature (pig farm vs. wild boar, detection method, etc.). From day 80, this time series was also provided with subsets (in the fenced area, in the buffer around the fences, and outside).
  - `TimeSeries_hunt_negative_test_day_<N>.csv`, a specific file for wild boar that tested negative after being shot by hunters (large file)
  - additional figures (cumulative incidence, maps) to illustrate the evolution of the situation, possibly with small errors.


Running model M0
----------------

1. Install EMULSION

   The installation procedure for EMULSION is fully described on [the software website](https://sourcesup.renater.fr/www/emulsion-public/Install.html). All simulations were carried out under Linux with Python 3.8 and `numpy` 1.18. **Install EMULSION version 1.1rc5** which was used to produce the synthetic data used for the ASF Challenge and discussed in the article. Ensure that your `PYTHONPATH` variable is set properly (i.e. containing current repository `.`) as explained in the installation instructions.

2. Clone the git repository and move into the `model/` directory:

   ```bash
   git clone https://forgemia.inra.fr/spicault/asf-challenge-2020.git
   cd asf-challenge-2020/model
   ```

3. Prepare input data

   Input data for the ASF model is stored by default in the `data/` directory. The git repository contains all necessary files, except for the exponential kernels and neighbourhood matrices used for calculating the forces of infection in the different transmission pathways. These files must be rebuilt before running the model. The trunctation of the kernels (set to 0 below a threshold) make it possible to discretize the whole area into 15x15 km$`^2`$ tiles. Beware, this step maybe time-consuming, and it produces 8.2 Gb data.

   ```bash
   cd data
   ./create_tiles.sh  ## beware! slow operation! produces 2948 files:
   ls tiles_15000     ## kernel_{boars,other,static}*.npz and neighbours*.npz
   cd ..
   ```

4. Run simulations

	The parameter values defined by default in the model file (`ppa.yaml`) already correspond to the initial conditions and to the control measures implemented in the trajectory selected for the Challenge.
	Hence, to reproduce this trajectory, use the following command (1 stochatic repetition with the specified random seed):


   ```bash
   # assuming that you are currently located in directory 'model':
   emulsion run ppa.yaml -r 1 --seed 23289
   ```

	This will produce 3 files in directory `outputs/`:

	- `log.txt` which contains each change occurring in epidemiological units and was used to produce the time series given to the Challenge participants
    - `herd_status.txt` which tracks pig farms in protection/surveillance areas or traced as suspicious trade contacts
	- `wildboars.txt` which contains statistics on wild boar per 15x15 km$`^2`$ tile

	These files should be the same as those are already provided in directory `outputs/challenge`.
