"""This file is aimed at providing specific code add-on for the PPA
model (ppa.yaml). It is based on the code skeletton generated
automatically by EMULSION.


emulsion run ppa.yaml --output-dir outputs/test --no-count -r 1 -t 10 -p nb_infected_wild_boars=1 -p transmission_boar_pig=0.03

"""

import sys
from   pathlib               import Path
import csv
import numpy                 as     np
import scipy.sparse          as     sp
from   scipy.spatial         import distance_matrix
import pandas                as     pd

from emulsion.agent.managers import CompartProcessManager
from emulsion.agent.managers import MetapopProcessManager
from emulsion.agent.views    import AutoStructuredView
from emulsion.tools.functions import random_bool

#===============================================================
# CLASS Metapop (LEVEL 'metapop')
#===============================================================
class Metapop(MetapopProcessManager):
    """
    Level of the spatial population.

    """

    #----------------------------------------------------------------
    # Level initialization
    #----------------------------------------------------------------

    def initialize_level(self, **others):
        """Initialize a metapop composed of pig herds and wild boars.

        BEWARE: data files for herds and boars must have an index
        column starting at 0 and incremented sequentially

        """
        # set of tiles surrounded by fences
        self.surrounded_tiles = set()
        # set of tiles outside fences but just adjacent to those within fences
        self.surrounding_tiles = set()
        # array of indexes of wildboars within fences
        self.wildboars_within_fences = np.array([], dtype='int32')
        # array of indexes of wildboars within fences
        self.wildboars_within_buffer = np.array([], dtype='int32')
        ## dictionary {step -> list of epidunits} for reactivating agents at specific time
        self.activation_schedule = {}
        ## dictionary {step -> list of epidunits} for preventive culling
        self.culling_schedule = {}
        self.init_epid_unit_data()
        self.create_initial_epidunits()
        self.init_trade()
        ## init fences location from data
        self.setup_fences()


    def init_epid_unit_data(self):
        print('Loading pig herd info')
        # load pig herds info
        herd_file = self.model.input_dir.joinpath('herds.csv')
        self.herds = pd.read_csv(herd_file, sep=',', header=0)
        self.nb_herds = self.herds.shape[0]
        print('Nb pig herds:', self.nb_herds)

        print('Loading wildboar info')
        # load wildboars info
        boar_file = self.model.input_dir.joinpath('boars.csv')
        self.wildboars = pd.read_csv(boar_file, sep=',', header=0)
        self.nb_wildboars = self.wildboars.shape[0]
        print('Nb wildboars:', self.nb_wildboars)

        # index -> healthy carcass there ?
        self.boars_natural_carcass = np.zeros(self.nb_wildboars)
        # index -> no more wildboar here (hunt or carcass removal)
        self.removed_wildboars = np.zeros(self.nb_wildboars)

        ## use pig_herd sizes to know how many susceptible animals are
        ## present in each herd, and other associated pre-computed
        ## information used to calculate infection probabilities
        print("Structuring herd biosecurity/outdoor information")
        self.susceptibles = self.herds['size'].values.reshape((1, self.nb_herds))
        self.biorisk = sp.csr_matrix(1 - self.herds['biosecurity'].values)
        self.biorisk_outdoor = sp.csr_matrix(self.herds['is_outdoor'].values).multiply(self.biorisk)

        print("Computing tiles")
        self.points = np.concatenate([self.herds[['X', 'Y']].values, self.wildboars[['X', 'Y']].values])
        self.Xmin, self.Ymin = self.points.min(axis=0)
        self.Xmax, self.Ymax = self.points.max(axis=0)

        self.C = int(self.get_model_value('resolution'))
        self.W = int(np.ceil((self.Xmax - self.Xmin) / self.C))
        self.H = int(np.ceil((self.Ymax - self.Ymin) / self.C))
        self.herds['tile'] = self.W * (np.floor((self.herds['Y'] - self.Ymin) / self.C)).astype(int) +\
                             (np.floor((self.herds['X'] - self.Xmin) / self.C)).astype(int)

        self.wildboars['tile'] = self.W * (np.floor((self.wildboars['Y'] - self.Ymin) / self.C)).astype(int) +\
                                 (np.floor((self.wildboars['X'] - self.Xmin) / self.C)).astype(int)

        tiles = set(self.herds['tile'])
        tiles |= set(self.wildboars['tile'])
        print(len(tiles), "distinct tiles in simulation environment")
        self.tiles_info = pd.DataFrame({'tile': self.wildboars['tile'].unique()})
        self.tiles_info['active'] = 0
        self.tiles_info.set_index(['tile'], inplace=True)

        # initial kernels (0)
        self.kernel_other_pathways = sp.csr_matrix((self.nb_herds, self.nb_herds))
        self.kernel_boar_static = sp.csr_matrix((self.nb_herds+self.nb_wildboars, self.nb_herds+self.nb_wildboars))
        self.kernel_boar_to_boar = sp.csr_matrix((self.nb_wildboars, self.nb_wildboars))

        # initial matrix to find neighbours within a given radius
        self.neighbours = sp.lil_matrix((self.nb_herds, self.nb_herds), dtype='int32')

        # date where suspicion due to trade contacts ends (population_index -> date to end suspicion)
        self.traced_end = sp.lil_matrix((1, self.nb_herds))
        # date where protection area ends (population_index -> date to end suspicion)
        self.protection_end = sp.lil_matrix((1, self.nb_herds))
        # date where surveillance area ends (population_index -> date to end suspicion)
        self.surveillance_end = sp.lil_matrix((1, self.nb_herds))

        # ## reset hunting bag if first run
        # self.hunting_bag_name= Path(self.log_path()).with_name('hunting_bag.txt')
        # if self.hunting_bag_name.exists() and self.statevars.simu_id==0:
        #     self.hunting_bag_name.unlink()

        ## reset log of removed trade movements if first run
        self.trade_log_name= Path(self.log_path()).with_name('trade_log.txt')
        if self.trade_log_name.exists() and self.statevars.simu_id==0:
            self.trade_log_name.unlink()

        ## reset log of herd status (P protection, S surveillance, T traced) if first run
        self.status_log_name= Path(self.log_path()).with_name('herd_status.txt')
        if self.status_log_name.exists() and self.statevars.simu_id==0:
            self.status_log_name.unlink()

        ## reset detailed WB log if first run
        self.wblog_name= Path(self.log_path()).with_name('wildboars.txt')
        if self.wblog_name.exists() and self.statevars.simu_id==0:
            self.wblog_name.unlink()

        ## initialize schedules for active wildboar carcass search
        self.search_schedule={} # step -> list of population_ID of centers of research areas
        self.wb_points = self.points[self.nb_herds:, :]

    def tile(self, col, row):
        """Return the number of the tile at column *col* and row *row*."""
        return col + self.W * row

    def coords(self, tile):
        """Return the coordinates (column, row) of the specified tile
        number.

        """
        return (tile % self.W, tile // self.W)

    def is_adjacent(self, tile1, tile2):
        """Test if *tile1* and *tile2* are adjacent."""
        col1, row1 = self.coords(tile1)
        col2, row2 = self.coords(tile2)
        return max(abs(col1 - col2), abs(row1 - row2)) == 1

    def customize_pigherd_prototype(self, pop_id, nb_exposed):
        proto = self.model.get_prototype('epid_unit', 'pig_herd')
        proto['initial_exposed'] = nb_exposed
        proto['is_infected'] = 1
        proto['is_traced'] = self.traced_pig_herds[0, pop_id-1]
        proto['is_in_protection_area'] = self.protection_pig_herds[0, pop_id-1]
        proto['is_in_surveillance_area'] = self.surveillance_pig_herds[0, pop_id-1]
        proto['date_traced_end'] = self.traced_end[0, pop_id-1]
        proto['date_protection_end'] = self.protection_end[0, pop_id-1]
        proto['date_surveillance_end'] = self.surveillance_end[0, pop_id-1]
        proto['changed'] = 1
        herd = self.herds[self.herds['population_id'] == pop_id]
        for field in ['population_id', 'X', 'Y', 'size', 'biosecurity', 'is_outdoor', 'is_commercial', 'tile']:
            proto[field] = float(herd[field])
        return proto

    def customize_wildboar_prototype(self, pop_id):
        proto = self.model.get_prototype('epid_unit', 'wild_boar')
        proto['initial_exposed'] = 1
        proto['is_infected'] = 1
        proto['size'] = 1
        proto['changed'] = 1
        boars = self.wildboars[self.wildboars['population_id'] == pop_id]
        for field in ['population_id', 'X', 'Y', 'tile']:
            proto[field] = float(boars[field])
        return proto

    def create_initial_epidunits(self):
        # list of active tiles (loaded)
        self.tiles = set()
        # list of tiles for which the pig herd dist matrix has been loaded
        self.neigh_tiles = set()
        ## INIT VECTORS AND OTHER VARIABLES USED TO REPRESENT RELEVANT HEALTH STATES/PREVALENCES
        self.statevars.initial_nb_boars_alive = self.get_model_value('nb_wildboars')
        self.reset_status()
        self.contribution_boar_to_pig = sp.csr_matrix((1, self.nb_herds))
        self.contribution_other_pathways = sp.csr_matrix((1, self.nb_herds))
        self.contribution_boar_to_boar = sp.csr_matrix((1, self.nb_wildboars))
        self.contribution_pig_to_boar = sp.csr_matrix((1, self.nb_wildboars))

        self.detected_pig_herds = sp.lil_matrix((1, self.nb_herds), dtype='int32')
        self.protection_pig_herds = sp.lil_matrix((1, self.nb_herds), dtype='int32')
        self.surveillance_pig_herds = sp.lil_matrix((1, self.nb_herds), dtype='int32')
        self.traced_pig_herds = sp.lil_matrix((1, self.nb_herds), dtype='int32')

        # CREATE INITIALLY INFECTED EPID UNITS
        nb_herds = int(self.get_model_value('nb_infected_pig_herds'))
        new_herds = [] if nb_herds == 0 else\
                    [(int(self.get_model_value('infected_pig_herd_ID')), 1)]
        nb_boars = int(self.get_model_value('nb_infected_wild_boars'))
        new_boars = [] if nb_boars == 0 else\
                    [int(self.get_model_value('infected_boar_ID'))]
        self.add_new_epidunits(new_herds, new_boars)

        # print('Calculating initial contributions')
        # self.compute_contributions_from_kernels()

    def add_new_epidunits(self, herd_list, boar_list):
        new_epid_units = [self.new_atom(custom_prototype=self.customize_pigherd_prototype(herd_id, nb_E), execute_actions=False)
                          for (herd_id, nb_E) in herd_list]
        new_epid_units += [self.new_atom(custom_prototype=self.customize_wildboar_prototype(boar_id), execute_actions=False)
                           for boar_id in boar_list]
        self.add_atoms(new_epid_units)

        new_tiles = set()
        for agent in new_epid_units:
            agent.reapply_prototype(execute_actions=True)
            # agent.log_custom_vars()
            new_tiles.add(agent.statevars.tile)
        tiles_to_add = new_tiles - self.tiles
        for tile in tiles_to_add:
            self.load_tile(tile)

    def load_tile(self, new_tile):
        # load kernels and integrate them to existing data
        # print('Loading tile', new_tile)
        # OTHER PATHWAYS (between herds through indirect contacts)
        print('Loading new tile', new_tile)
        alpha = self.get_model_value('alpha_other_pathways')
        kop_file = self.model.input_dir.joinpath('tiles_{}'.format(self.C),
                                                 'kernel_other_{}_{}.npz'.format(int(alpha), int(new_tile)))
        # print('Processing', kop_file)
        self.kernel_other_pathways += sp.load_npz(kop_file)
        # new_kernel = sp.load_npz(kop_file).tolil()
        # idx = new_kernel.nonzero()
        # self.kernel_other_pathways[idx] = new_kernel[idx]

        # BOAR-PIG HERD RELATIONSHIPS
        alpha = self.get_model_value('alpha_boar_static')
        kbs_file = self.model.input_dir.joinpath('tiles_{}'.format(self.C),
                                                 'kernel_static_{}_{}.npz'.format(int(alpha), int(new_tile)))
        # print('Processing', kbs_file)
        self.kernel_boar_static += sp.load_npz(kbs_file)
        # new_kernel = sp.load_npz(kbs_file).tolil()
        # idx = new_kernel.nonzero()
        # self.kernel_boar_static[idx] = new_kernel[idx]


        # BOAR-BOAR RELATIONSHIPS
        alpha = self.get_model_value('alpha_boar_boar')
        kbb_file = self.model.input_dir.joinpath('tiles_{}'.format(self.C),
                                                 'kernel_boars_{}_{}.npz'.format(int(alpha), int(new_tile)))
        # print('Processing', kbb_file)
        self.kernel_boar_to_boar += sp.load_npz(kbb_file)
        # new_kernel = sp.load_npz(kbb_file).tolil()
        # idx = new_kernel.nonzero()
        # self.kernel_boar_to_boar[idx] = new_kernel[idx]

        self.tiles.add(new_tile)
        self.tiles_info['active'][new_tile] = 1

        # if fences have been installed, if new tile is the neighbour of a sourrounded tile, modify the corresponding kernel
        if self.get_model_value('use_fences') and self.statevars.date_first_detection > 0 and self.time >= self.get_model_value('delay_use_fences') + self.statevars.date_first_detection:
            if new_tile in self.surrounding_tiles:
                print('Adapting kernel for new tile in buffer area', new_tile)
                self.kernel_update_fences([new_tile], self.surrounded_tiles)
            if new_tile in self.surrounded_tiles:
                print('Adapting kernel for new tile within fences', new_tile)
                self.kernel_update_fences([new_tile], self.surrounding_tiles)


    def load_neighbours(self, new_tile):
        print('Loading neighbourhood for tile', new_tile)
        if new_tile not in self.neigh_tiles:
            #### LOAD neighbours only when needed
            # NEIGHBOURHOOD RELATIONSHIPS
            neighb_file = self.model.input_dir.joinpath('tiles_{}'.format(self.C),
                                                        'neighbours_{}.npz'.format(int(new_tile)))
            print('Processing', neighb_file)
            #### remember that matrix is triangular in the part concerning
            #### each tile (we only need to find who is at distance D of
            #### another epidunit => look at one row/col in matrix without
            #### making the matrix symmetric)
            self.neighbours += sp.load_npz(neighb_file)
            self.neigh_tiles.add(new_tile)


    def init_trade(self):
        ## TRADE

        # read a CSV data file for moves:
        # date of movement, source herd, destination herd, quantity

        # and restructure it according to origin_date and delta_t:
        # {step: {source_id: [(dest_id, qty), ...],
        #         ...},
        #  ...}
        origin = self.model.origin_date
        step_duration = self.model.step_duration
        moves = {}
        move_file = Path(self.model.input_dir, 'moves.csv')
        print("Processing", move_file)
        with open(move_file) as csvfile:
            # read the CSV file
            csvreader = csv.DictReader(csvfile, delimiter=',')
            for row in csvreader:
                # read date and incorporate the offset between user
                # time (0=beginning of hunting period) and simulation
                # time (0 = a few days before)
                day = int(row['date'])
                # if day < 0:
                #     # ignore dates before simulation
                #     continue
                # convert dates into simulation steps
                # step = (day - origin) // step_duration
                step = day // step_duration.days
                # group information by step and source herd
                if step not in moves:
                    moves[step] = {}
                src, dest, qty = int(row['source']), int(row['dest']), int(row['qty'])
                if src != dest:
                    if src not in moves[step]:
                        moves[step][src] = []
                    moves[step][src].append([dest, qty])
        self.moves = moves


    def log_active_search(self, cases, healthy):
        """Store custom information for WB carcasses found in active search.

        *cases* and *healthy* represent indexes of respectively infected and healthy carcasses
        """
        ## logged variables:
        ## simu_id, step, population_id, is_infected, is_detected, date_detected, is_traced, date_traced_end, is_in_protection_area, date_protection_end, is_in_surveillance_area, date_surveillance_end, is_hunted, is_tested, S, E, I, C, D
        dur_search = int(self.get_model_value('duration_carcass_active_search'))
        message = ''
        values = [
            [ self.statevars.simu_id,
              self.statevars.step,
              pop_idx + self.nb_herds + 1,
              1, ## is_infected,
              0, ## is_detected,
              1, ## is_searched,
              0, ## is_culled
              self.statevars.step - np.random.random_integers(0, dur_search), ## date_detected,
              0, ## is_traced,
              0, ## date_traced_end,
              0, ## is_in_protection_area,
              0, ## date_protection_end,
              0, ## is_in_surveillance_area,
              0, ## date_surveillance_end,
              0, ## is_hunted,
              0, ## is_tested,
              0, ## total_S,
              0, ## total_E,
              0, ## total_I,
              1, ## total_C,
              0, ## total_D,
            ] for pop_idx in cases
        ]

        values += [
            [ self.statevars.simu_id,
              self.statevars.step,
              pop_idx + self.nb_herds + 1,
              0, ## is_infected,
              0, ## is_detected,
              1, ## is_searched,
              0, ## is_culled
              self.statevars.step - np.random.random_integers(0, dur_search), ## date_detected,
              0, ## is_traced,
              0, ## date_traced_end,
              0, ## is_in_protection_area,
              0, ## date_protection_end,
              0, ## is_in_surveillance_area,
              0, ## date_surveillance_end,
              0, ## is_hunted,
              0, ## is_tested,
              0, ## total_S,
              0, ## total_E,
              0, ## total_I,
              0, ## total_C,
              1, ## total_D,
            ] for pop_idx in healthy
        ]

        if any(values):
            nb_fields = len(values[0])
            message = '\n'.join((','.join(['{}'] * nb_fields)).format(*row) for row in values)

        with open(self.log_path(), 'a') as logfile:
            logfile.write('{}\n'.format(message))


    def log_preventive_culling(self, healthy_idx):
        """Store custom information for healthy pig herds (list of indices)
        culled preventively (infected pig herds culled preventively
        even when not detected are in charge of logging their own
        variables since they are running in the simulation).

        """
        ## logged variables:
        ## simu_id, step, population_id, is_infected, is_detected, date_detected, is_traced, date_traced_end, is_in_protection_area, date_protection_end, is_in_surveillance_area, date_surveillance_end, is_hunted, is_tested, S, E, I, C, D
        message = ''
        values = [
            [ self.statevars.simu_id,
              self.statevars.step,
              idx+1, # population_id
              0, ## is_infected,
              0, ## is_detected,
              0, ## is_searched,
              1, ## is_culled,
              self.statevars.step, ## date culled,
              self.traced_pig_herds[0, idx], ## is_traced,
              self.traced_end[0, idx], ## date_traced_end,
              self.protection_pig_herds[0, idx], ## is_in_protection_area,
              self.protection_end[0, idx], ## date_protection_end,
              self.surveillance_pig_herds[0, idx], ## is_in_surveillance_area,
              self.surveillance_end[0, idx], ## date_surveillance_end,
              0, ## is_hunted,
              0, ## is_tested,
              0, ## total_S,
              0, ## total_E,
              0, ## total_I,
              0, ## total_C,
              0, ## total_D,
            ] for idx in healthy_idx
        ]

        if any(values):
            nb_fields = len(values[0])
            message = '\n'.join((','.join(['{}'] * nb_fields)).format(*row) for row in values)

        with open(self.log_path(), 'a') as logfile:
            logfile.write('{}\n'.format(message))

    def log_hunted_wildboars(self, healthy_idx):
        """Store custom information for tested hunted wildboars (list of
indices).

        """
        ## logged variables:
        ## simu_id, step, population_id, is_infected, is_detected, date_detected, is_traced, date_traced_end, is_in_protection_area, date_protection_end, is_in_surveillance_area, date_surveillance_end, is_hunted, is_tested, S, E, I, C, D
        nb_PH = self.nb_herds
        message = ''
        values = [
            [ self.statevars.simu_id,
              self.statevars.step,
              idx + nb_PH + 1, # population_id
              0, ## is_infected,
              0, ## is_detected,
              0, ## is_searched,
              0, ## is_culled,
              self.statevars.step, ## date hunted,
              0, ## is_traced,
              0, ## date_traced_end,
              0, ## is_in_protection_area,
              0, ## date_protection_end,
              0, ## is_in_surveillance_area,
              0, ## date_surveillance_end,
              1, ## is_hunted,
              1, ## is_tested,
              0, ## total_S,
              0, ## total_E,
              0, ## total_I,
              0, ## total_C,
              0, ## total_D,
            ] for idx in healthy_idx
        ]

        if any(values):
            nb_fields = len(values[0])
            message = '\n'.join((','.join(['{}'] * nb_fields)).format(*row) for row in values)

        with open(self.log_path(), 'a') as logfile:
            logfile.write('{}\n'.format(message))



    #----------------------------------------------------------------
    # Statevars
    #----------------------------------------------------------------
    @property
    def nb_boars_alive(self):
        return self.statevars.initial_nb_boars_alive - self.statevars.nb_deceased_wildboars

    #----------------------------------------------------------------
    # Processes
    #----------------------------------------------------------------

    def change_seed(self):
      """ Use a new seed for the rest of the simulation
      """
      if self.get_model_value('use_second_seed') and self.statevars.date_first_detection > 0 and self.time == self.get_model_value('delay_change_seed') + self.statevars.date_first_detection:
          np.random.seed(int(self.get_model_value('second_seed')))

    def setup_fences(self):
        """Identify location of future physical barriers between tiles specified in data (fences.csv and buffer.csv).

        """
        if self.get_model_value('use_fences'):
            # read explicit list of tiles to enclose
            fences_file = self.model.input_dir.joinpath('fences.csv')
            self.surrounded_tiles = set(pd.read_csv(fences_file, sep=',', header=0)['tile'])
            buffer_file = self.model.input_dir.joinpath('buffer.csv')
            self.surrounding_tiles = set(pd.read_csv(buffer_file, sep=',', header=0)['tile'])

            # identify indexes of wildboars within tiles
            self.wildboars_within_fences = self.wildboars[self.wildboars['tile'].isin(self.surrounded_tiles)].index
            self.wildboars_within_buffer = self.wildboars[self.wildboars['tile'].isin(self.surrounding_tiles)].index

    def install_fences(self):
        """Install physical barriers between predefined surrounded/surrounding tiles. Kernel values can only be modified for surrounded/surrounding tiles already active. When new tiles are loaded due to infection spread, kernels must be modified consequently.

        """
        ### TODO: prevent installing fences while not yet detected (temporarily allowed for test purpose)
        if self.get_model_value('use_fences') and self.statevars.date_first_detection > 0 and self.time == self.get_model_value('delay_use_fences') + self.statevars.date_first_detection: # and self.statevars.date_first_detection > 0
            nb_active_tiles = len(self.surrounded_tiles)
            area = nb_active_tiles * (self.get_model_value('resolution') / 1000)**2
            print('INSTALLING FENCES at t =', self.statevars.step, 'for', nb_active_tiles, 'tiles =', area, 'km2')
            print('SURROUNDED TILES:', self.surrounded_tiles)
            print('SURROUNDING TILES:', self.surrounding_tiles)

            self.kernel_update_fences(self.surrounded_tiles, self.surrounding_tiles)
            self.kernel_update_fences(self.surrounding_tiles, self.surrounded_tiles)
            self.statevars.changed = True

    def check_if_finished(self):
        """Stop simulation if time >= date detection + game duration (enough data
        for players) if a game duration > 0 is set.

        """
        game_duration = self.get_model_value('game_duration')
        if game_duration > 0 and self.statevars.date_first_detection > 0 and self.time >= self.statevars.date_first_detection + game_duration:
            print('HALTING SIMULATION AT', self.time)
            self.deactivate()

    def kernel_update_fences(self, from_tiles, to_tiles):
        """Modify boar_static and boar_to_boar kernels between tiles contained
        in *from_tiles* and tiles contained in *to_tiles*, to account
        for newly installed or existing fences. The operation is not
        symmetric. Two first calls (symmetrical) of this method must be done when
        installing fences, then when a new tile is loaded, if it is in the list
        of surrounded (resp. surrounding) tiles, the method must be
        called with from_tiles = new tile and to_tiles = surrounding (resp.
        surrounded) tiles.

        """
        # retrieve parameter value
        efficacy = self.get_model_value('fences_efficacy')
        ## iteration on source tiles to limit memory footprint !
        for source_tile in from_tiles:
            ## skip non-active source tiles
            if not self.tiles_info['active'][source_tile]:
                continue
            ## skip non-adjacent tiles since kernels between
            ## non-adjacent tiles is already 0
            adjacent_tiles = set(dest_tile for dest_tile in to_tiles
                                 if self.is_adjacent(source_tile, dest_tile))
            if len(adjacent_tiles) == 0:
                continue
            print('\t- between', source_tile, 'and', adjacent_tiles)
            # retrieve pig herds and wild boars on each series of tiles
            local_ph = self.herds[self.herds['tile'] == source_tile]
            local_wb = self.wildboars[self.wildboars['tile'] == source_tile]
            neighbour_ph = self.herds[self.herds['tile'].isin(adjacent_tiles)]
            neighbour_wb = self.wildboars[self.wildboars['tile'].isin(adjacent_tiles)]

            # retrieve corresponding population_id
            local_ph_id = local_ph['population_id'].values
            local_wb_id = local_wb['population_id'].values
            neighbour_ph_id = neighbour_ph['population_id'].values
            neighbour_wb_id = neighbour_wb['population_id'].values
            # transform into indexes
            ## for kernel_boar_static : all epid units
            local_units_idx = np.concatenate([local_ph_id, local_wb_id]) - 1
            neighbour_units_idx = np.concatenate([neighbour_ph_id, neighbour_wb_id]) - 1
            urow, ucol = np.meshgrid(local_units_idx, neighbour_units_idx)
            urow, ucol = urow.T, ucol.T
            ## for kernel_boar_boar: only wildboars
            local_wb_idx = local_wb_id - self.nb_herds - 1
            neighbour_wb_idx = neighbour_wb_id - self.nb_herds - 1
            wbrow, wbcol = np.meshgrid(local_wb_idx, neighbour_wb_idx)
            wbrow, wbcol = wbrow.T, wbcol.T

            ## modify kernel
            # retrieve values of kernel for local/neighbour epidunits and multiply by efficacy
            kbs_extract = self.kernel_boar_static.tolil()[urow, ucol] * efficacy
            # put resulting values in larger sparse matrix
            diff = sp.lil_matrix((self.nb_herds + self.nb_wildboars, self.nb_herds + self.nb_wildboars))
            diff[urow, ucol] = kbs_extract
            # remove values from existing kernel
            self.kernel_boar_static -= diff.tocsr()
            # same for boar/boar kernel
            kbb_extract = self.kernel_boar_to_boar.tolil()[wbrow, wbcol] * efficacy
            diff = sp.lil_matrix((self.nb_wildboars, self.nb_wildboars))
            diff[wbrow, wbcol] = kbb_extract
            self.kernel_boar_to_boar -= diff.tocsr()


    def distant_wildboar_mortality(self):
        """Represent natural mortality and hunt for wildboars not present in
        the simulation (hence reducing the capability of the pathogen
        to spread across time during hunting season).

        Wildboars represented as explicit epidemiological units in the
        simulation are already subject to these processes.

        """
        # retrieve index of wildboars not in simulation
        # retrieve population_id of epidemiological units in simulation
        pop = self.get_populations().values()
        idx_in_sim = np.array([unit.index for unit in pop if not unit.statevars.is_pig_herd])
        idx = np.arange(self.nb_wildboars)
        idx = np.setdiff1d(idx, idx_in_sim)
        idx = np.setdiff1d(idx, self.boars_infectious_carcass.nonzero()[0])

        # first remove natural carcasses if not simulated explicitly
        carcass_idx = self.boars_natural_carcass.nonzero()[0]
        if carcass_idx.any():
            idx = np.setdiff1d(idx, carcass_idx)
            removal_rate = self.get_model_value('wildboar_C_removal_rate')
            p_removal = 1 - np.exp(- removal_rate * self.model.delta_t)
            nb_removed = np.random.binomial(len(carcass_idx), p_removal)
            np.random.shuffle(carcass_idx)
            removed = carcass_idx[:nb_removed]
            self.boars_natural_carcass[removed] = 0
            self.removed_wildboars[removed] = 1

        # remove remaining dead wildboars (hunted or removed carcasses)
        idx = np.setdiff1d(idx, self.removed_wildboars.nonzero()[0])

        ## if increased hunting is implemented within installed fences, handle areas separately
        if self.get_model_value('ongoing_hunting_within_fences'):
            fences_idx = np.intersect1d(idx, self.wildboars_within_fences)
            idx = np.setdiff1d(idx, fences_idx)
        else:
            fences_idx = np.array([], dtype='int32')

        ## if increased hunting is implemented around installed fences, handle areas separately
        if self.get_model_value('ongoing_hunting_within_buffer'):
            buffer_idx = np.intersect1d(idx, self.wildboars_within_buffer)
            idx = np.setdiff1d(idx, buffer_idx)
        else:
            buffer_idx = np.array([], dtype='int32')

        natural_dead = np.array([], dtype='int32')
        hunted = np.array([], dtype='int32')

        for wildboar_idx, hunting_parameter, test_parameter in [(idx, 'default', 'default'), (fences_idx, 'fences', 'increased'), (buffer_idx, 'buffer', 'increased')]:
            if not any(wildboar_idx):
                ## skip if empty
                continue
            # natural mortality
            natural_death_rate = self.get_model_value('wildboar_death_rate')
            # hunting rate (for regular hunting, 0 outside hunting period)
            hunting_rate = self.get_model_value('hunting_rate_{}'.format(hunting_parameter))
            ## transform rates into probabilities
            death_rate = natural_death_rate + hunting_rate
            p_death = 1 - np.exp(- death_rate * self.model.delta_t)
            p_hunt = p_death * hunting_rate / death_rate
            p_natural = p_death * natural_death_rate / death_rate
            nb_natural, nb_hunted, _ = np.random.multinomial(len(wildboar_idx), [p_natural, p_hunt, 1-p_death])
            np.random.shuffle(wildboar_idx)
            natural_dead = np.concatenate([natural_dead, wildboar_idx[:nb_natural]])
            hunted = wildboar_idx[nb_natural:(nb_natural + nb_hunted)]

            self.removed_wildboars[hunted] = 1

            if nb_hunted > 0 and self.statevars.date_first_detection > 0:
                proba_test = self.get_model_value('proba_detection_hunted_wildboars_{}'.format(test_parameter))
                nb_tested = np.random.binomial(hunted.size, proba_test)
                np.random.shuffle(hunted)
                tested_negative = hunted[:nb_tested]
                # if nb_tested > 0:
                #     print(self.statevars.step, 'Hunted WB:', nb_hunted,'- tested negative:', nb_tested)
                self.log_hunted_wildboars(tested_negative)
                print('Hunted', hunting_parameter, nb_hunted, 'tested -:', nb_tested)

        self.boars_natural_carcass[natural_dead] = 1



    def active_carcass_search(self):
        """Perform an active search for healthy or infected wildboar carcasses
        in an area centered around discovered infected carcasses.

        """
        if self.statevars.step in self.search_schedule:
            ## radius of the circles
            radius = self.get_model_value('radius_boar_detection_carcass_active')
            ## transform list of population_IDs into list of WB index
            wb_centers_idx = np.unique(np.array(self.search_schedule[self.statevars.step], dtype=int)) - (1 + self.nb_herds)
            ## if ongoing increased hunting, cancel active carcass search around carcasses in the concerned area
            if self.get_model_value('ongoing_hunting_within_fences'):
                wb_centers_idx = np.setdiff1d(wb_centers_idx, self.wildboars_within_fences)
            if self.get_model_value('ongoing_hunting_within_buffer'):
                wb_centers_idx = np.setdiff1d(wb_centers_idx, self.wildboars_within_buffer)

            if any(wb_centers_idx):
                ## identify WB on the same tile within a radius from the cases
                wb_tiles = np.unique(self.wildboars['tile'][wb_centers_idx].values)
                print('At {}, active carcass search on tiles {} (radius {} m)'.format(self.statevars.step, wb_tiles, radius))
                wb_in_tiles_idx = np.concatenate(
                    [(self.wildboars['tile']==tile).to_numpy().nonzero()[0]
                     for tile in wb_tiles]
                )
                ## coordinates of centers
                centers = self.wb_points[wb_centers_idx, :]
                ## coordinates of WB of the tiles
                others = self.wb_points[wb_in_tiles_idx, :]
                ## distance matrix between both
                dist_matrix = distance_matrix(others, centers)
                ## index of points of the tiles within the radius
                points_in_radius_idx = np.unique(np.nonzero(dist_matrix <= radius)[0])
                ## index of WB of the tiles within the radius
                WB_in_radius_idx = wb_in_tiles_idx[points_in_radius_idx]
                ## separate simulated WB from implicit ones
                population = self.get_populations()
                # list of WB index already in simulation
                simulated_WB = [ pop_idx
                                 for pop_idx in WB_in_radius_idx
                                 if pop_idx + (self.nb_herds+1) in population]
                non_simulated_WB = np.setdiff1d(WB_in_radius_idx, simulated_WB)
                ## search for infected WB carcasses among simulated ones
                proba_det_active = self.get_model_value('proba_detection_carcass_active_search')
                simulated_WB_carcasses = [ pop_idx
                                           for pop_idx in simulated_WB
                                           if population[pop_idx + (self.nb_herds+1)].total_C > 0]
                nb_found = np.random.binomial(len(simulated_WB_carcasses), proba_det_active)
                if nb_found > 0:
                    np.random.shuffle(simulated_WB_carcasses)
                    cases_found = simulated_WB_carcasses[:nb_found]
                else:
                    cases_found = np.array([], dtype=int)
                print('At {}, {} infected carcasses found around {}:\n{}'.format(self.statevars.step, nb_found, wb_centers_idx, cases_found))
                ## search for healthy WB carcasses among non-simulated ones
                non_simulated_WB_carcasses = np.intersect1d(non_simulated_WB, self.boars_natural_carcass.nonzero()[0])
                nb_found_non_simulated = np.random.binomial(len(non_simulated_WB_carcasses), proba_det_active)
                if nb_found_non_simulated > 0:
                    np.random.shuffle(non_simulated_WB_carcasses)
                    healthy_found = non_simulated_WB_carcasses[:nb_found_non_simulated]
                else:
                    healthy_found = np.array([], dtype=int)
                print('At {}, {} healthy carcasses found around {}:\n{}'.format(self.statevars.step, nb_found_non_simulated, wb_centers_idx, healthy_found))
                ## log information
                self.log_active_search(cases_found, healthy_found)
                ## remove all carcasses
                for pop_idx in cases_found:
                    pop_id = pop_idx + self.nb_herds + 1
                    boar = population[pop_id]
                    boar.statevars.is_detected = 1
                    boar.statevars.date_detected = self.statevars.step
                    boar.remove_all()
                    boar.statevars._is_active = False
                    boar.statevars.changed = False ## this to avoid logging (done separately)
                    boar.update_health_info()
                self.boars_natural_carcass[healthy_found] = 0
                self.removed_wildboars[healthy_found] = 1
                ## if new cases were found, schedule new searchs around
                if any(cases_found):
                    # do not schedule active search around carcasses found
                    # on tiles within fences or within buffer when
                    # increased hunting is ongoing
                    cases_to_search = cases_found
                    if self.get_model_value('ongoing_hunting_within_fences'):
                        cases_to_search = np.setdiff1d(cases_to_search, self.wildboars_within_fences)
                    if self.get_model_value('ongoing_hunting_within_buffer'):
                        cases_to_search = np.setdiff1d(cases_to_search, self.wildboars_within_buffer)
                    if any(cases_to_search):
                        ## schedule further carcass search
                        step = int(self.statevars.step + self.get_model_value('delay_carcass_active_search') + self.get_model_value('duration_carcass_active_search'))
                        if step not in self.search_schedule:
                            self.search_schedule[step] = np.array([], dtype='int32')
                        print('Scheduling active search around', cases_to_search, 'for time', step)
                        self.search_schedule[step] = np.concatenate([self.search_schedule[step], np.array(cases_to_search, dtype='int32') + 1 + self.nb_herds]) # retransform index into pop ID
                    ## check if any preventive culling in radius
                    if self.get_model_value('cull_near_infected_carcasses') and self.time >= self.get_model_value('delay_cull_near_infected_carcasses') + self.statevars.date_first_detection:
                        self.identify_pig_herds_near_carcass(cases_found)

        ## log number of WB non removed/naturally dead by tile
        wb_tiles = self.wildboars[['tile', 'population_id']]
        natural_carcasses = wb_tiles.iloc[self.boars_natural_carcass.nonzero()[0]]
        removed = wb_tiles.iloc[self.removed_wildboars.nonzero()[0]]
        boars_I = wb_tiles.iloc[self.boars_infectious_alive.nonzero()[0]]
        boars_C = wb_tiles.iloc[self.boars_infectious_carcass.nonzero()[0]]
        # count each category by tile
        nb_nat_carcasses = natural_carcasses.groupby(['tile']).count()
        nb_removed = removed.groupby(['tile']).count()
        nb_I = boars_I.groupby(['tile']).count()
        nb_C = boars_C.groupby(['tile']).count()
        ## TODO: left join de la liste des tuiles + count de chaque catégorie par tuile + tuile active ?
        tiles_stat = self.tiles_info.join(nb_nat_carcasses).join(nb_removed, rsuffix='R').join(nb_I, rsuffix='I').join(nb_C, rsuffix='C').fillna(0).rename(columns={"population_id": "D", "population_idR": "R", "population_idI": "I", "population_idC": "C"})
        tiles_stat['simu_id'] = self.statevars.simu_id
        tiles_stat['step'] = self.statevars.step
        with open(self.wblog_name, 'a') as logfile:
            ## TODO from pandas DF
            tiles_stat.to_csv(logfile, sep=',', header=(self.statevars.step == 0 and self.statevars.simu_id == 0), index=True)


    def reactivate_units(self):
        """Reactivate pig herds that were previously deactivated (eg. pig
        herds where all animals were removed), assuming they are still
        present in the metapop (but just deactivated). This also
        applies to non-simulated pig herds, e.g. culled as a
        preventive measure.

        """
        populations = self.get_populations()
        if self.statevars.step in self.activation_schedule:
            for herd_idx in self.activation_schedule[self.statevars.step]:
                is_in_protection_area = self.protection_pig_herds[0, herd_idx]
                date_protection_end = self.protection_end[0, herd_idx]
                if is_in_protection_area and date_protection_end > self.statevars.step:
                    # if in protection area reschedule reactivation
                    if date_protection_end not in self.activation_schedule:
                        self.activation_schedule[date_protection_end] = []
                    else:
                        self.activation_schedule[date_protection_end].append(herd_idx)
                else: # otherwise repopulate
                    size = self.herds[self.herds['population_id'] == herd_idx + 1]['size']
                    self.susceptibles[0, herd_idx] = size
                    print("Repopulating pig herd", herd_idx + 1, "at t =", self.statevars.step, "with nb =", size)
                    if herd_idx + 1 not in populations:
                        ## if herd is not simulated, log new state then skip other operations
                        self.log_repop_virtual(herd_idx)
                        continue
                    epid_unit = populations[herd_idx + 1]
                    epid_unit.statevars.is_in_protection_area = 0
                    epid_unit.statevars.date_protection_end = date_protection_end
                    epid_unit.statevars.is_in_surveillance_area = self.surveillance_pig_herds[0, epid_unit.statevars.population_id-1]
                    epid_unit.statevars.is_traced = self.traced_pig_herds[0, epid_unit.statevars.population_id-1]
                    epid_unit.statevars.date_traced_end = self.traced_end[0, epid_unit.statevars.population_id-1]
                    epid_unit.statevars.date_surveillance_end = self.surveillance_end[0, epid_unit.statevars.population_id-1]
                    epid_unit.statevars._is_active = True
                    epid_unit.statevars.changed = True
                    epid_unit.statevars.is_detected = 0
                    epid_unit.statevars.is_infected = 0
                    epid_unit.statevars.is_culled = 0
                    epid_unit.statevars.date_detected = 0
                    epid_unit.add_population({'infection': { (self.get_model_value('S'),): epid_unit.statevars.size}})
                    epid_unit.log_custom_vars()

    def log_repop_virtual(self, herd_idx):
        """Log pig herd repopulated while not in the simulation."""
        ## logged variables:
        ## simu_id, step, population_id, is_infected, is_detected, is_searched, date_detected, is_traced, date_traced_end, is_in_protection_area, date_protection_end, is_in_surveillance_area, date_surveillance_end, is_hunted, is_tested, S, E, I, C, D
        values = [
            self.statevars.simu_id,
            self.statevars.step,
            herd_idx + 1,   # population_id
            0, # is_infected,
            0, #is_detected,
            0, #is_searched,
            0, #is_culled,
            0, #date_detected,
            self.traced_pig_herds[0, herd_idx], #is_traced,
            self.traced_end[0, herd_idx], #date_traced_end,
            self.protection_pig_herds[0, herd_idx], #is_in_protection_area,
            self.protection_end[0, herd_idx], #date_protection_end,
            self.surveillance_pig_herds[0, herd_idx], # is_in_surveillance_area,
            self.surveillance_end[0, herd_idx], # date_surveillance_end,
            0, # is_hunted,
            0, # is_tested,
            self.susceptibles[0, herd_idx], # total_S,
            0, #total_E,
            0, #total_I,
            0, #total_C,
            0, #total_D,
        ]
        message = (','.join(['{}'] * len(values))).format(*values)
        with open(self.log_path(), 'a') as logfile:
            logfile.write('{}\n'.format(message))


    def pig_trade(self):
        """Proceed to host moves from source populations to dest
        populations.

        Assumptions:
        - the system is closed, thus individuals should not be sent
          outside the metapopulation or come from outside the
          metapopulation
        - however, not all epidemiological units are simulated, but
          only infected ones or those that have been infected

        Thus, the metapop has, on the one hand, to transfer messages
        from populations present in the simulation to others, and on
        the other hand, to generate messages from populations absent
        from the simulation to populations present in the simulation

        """
        populations = self.get_populations()
        step = self.statevars.step
        all_moves = self.moves
        ## using step-1 since sales are performed at the end of previous step !
        if step-1 in all_moves:
            for src in all_moves[step-1]:
                if src in populations:
                    ## handle existing source populations which
                    ## generated the messages to others themselves
                    outgoing = populations[src].get_outbox()
                    # print(step, src, outgoing)
                    if outgoing and populations[src].statevars._is_active:
                        for message in outgoing:
                            dest_ID = message['dest']
                            sell_infected = (message['hosts']['E'] + message['hosts']['I'] > 0)
                            if sell_infected:
                                print('selling infected animals', message)
                            if dest_ID in populations:
                                # dest population in the metapop
                                populations[dest_ID].add_inbox([message])
                            elif sell_infected:
                                # create new epidunit in the metapop in E/I animals sold
                                self.add_new_epidunits([(dest_ID, 0)], [])
                                populations = self.get_populations()
                                populations[dest_ID].add_inbox([message])
                                print('NEW PIG HERD INFECTED BY TRADE', dest_ID, 'at', self.statevars.step)
                            # otherwise individuals are just "lost"
                    # otherwise no message (not enough animals to sell
                    # or inactive pop)
                else:
                    ## susceptible pigs sold by "virtual" pig herd
                    ## (not currently in simulation)
                    if not (self.detected_pig_herds[0, src-1] or self.protection_pig_herds[0, src-1] or self.surveillance_pig_herds[0, src-1] or self.traced_pig_herds[0, src-1]):
                        for dest, qty in all_moves[step-1][src]:
                            if dest in populations and populations[dest].statevars._is_active and\
                               not (self.detected_pig_herds[0, dest-1] or self.protection_pig_herds[0, dest-1] or self.surveillance_pig_herds[0, dest-1] or self.traced_pig_herds[0, dest-1]) :
                                message = {'src': src, 'dest': dest, 'hosts': {'S': qty}}
                                populations[dest].add_inbox([message])
                                print('DEST', dest, 'RECEIVING', message, 'FROM virtual source', src)


    def compute_contributions_from_kernels(self, *args, **kwargs):
        """Python-defined function to compute the summations over infectious
        hosts based on the exponential kernels. The results are stored in
        variables named contrib_....

        """
        # reset contact status when traced/protection/surveillance ended
        _, ended_traced, _ = sp.find(self.traced_end[0,:] == self.statevars.step)
        _, ended_protection, _ = sp.find(self.protection_end[0,:] == self.statevars.step)
        _, ended_surveillance, _ = sp.find(self.surveillance_end[0,:] == self.statevars.step)

        if any(ended_traced):
            print("Removing from traced contacts", ended_traced)
            self.traced_pig_herds[0, ended_traced] = 0
            self.traced_end[0, ended_traced] = 0
            population = self.get_populations()
            for herd_index in ended_traced:
                if herd_index+1 in population:
                    population[herd_index+1].statevars.is_traced = 0
                    population[herd_index+1].statevars.changed = True

        if any(ended_protection):
            print("Removing from protection_area", ended_protection)
            self.protection_pig_herds[0, ended_protection] = 0
            self.protection_end[0, ended_protection] = 0
            population = self.get_populations()
            for herd_index in ended_protection:
                if herd_index+1 in population:
                    population[herd_index+1].statevars.is_in_protection_area = 0
                    population[herd_index+1].statevars.changed = True

        if any(ended_surveillance):
            print("Removing from surveillance area", ended_surveillance)
            self.surveillance_pig_herds[0, ended_surveillance] = 0
            self.surveillance_end[0, ended_surveillance] = 0
            population = self.get_populations()
            for herd_index in ended_surveillance:
                if herd_index+1 in population:
                    population[herd_index+1].statevars.is_in_surveillance_area = 0
                    population[herd_index+1].statevars.changed = True


        bia = sp.csr_matrix(self.boars_infectious_alive)
        pwpop = sp.csr_matrix(self.pig_herd_weighted_prevalence_other_pathways)
        bc = sp.csr_matrix(self.boars_infectious_carcass)
        pwp = sp.csr_matrix(self.pig_herd_weighted_prevalence)
        # contribution of infectious alive boar to pig herds
        # (1, WB).T(PH, WB) -> (1, PH)
        # self.contribution_boar_to_pig = bia.dot(self.kernel_boar_pig.T)
        # (1, WB).(WB, PH) -> (1, PH)
        self.contribution_boar_to_pig = bia.dot(self.kernel_boar_static[self.nb_herds:, :self.nb_herds])
        # print(self.contribution_boar_to_pig.shape)
        # contribution of other pathways to pig herds in herd-to-herd transmission
        # (1, PH).(PH, PH) -> (1, PH)
        self.contribution_other_pathways = pwpop.dot(self.kernel_other_pathways)
        # contribution of infectious alive boars and carcasses to boars
        # (1, WB).(WB, WB) -> (1, WB)
        # (1, WB).(WB, WB) -> (1, WB)
        # self.contribution_boar_to_boar = bia.dot(self.kernel_boar_to_boar) + bc.dot(self.kernel_carcass_to_boar)
        # ((1, WB) + (1, WB)).(WB, WB) -> (1, WB)
        # self.contribution_boar_to_boar = (bia + bc).dot(self.kernel_boar_to_boar)
        # ((1, WB).(WB, WB) + (1, WB).(WB, WB) -> (1, WB)
        self.contribution_boar_to_boar = bia.dot(self.kernel_boar_to_boar) + bc.dot(self.kernel_boar_static[self.nb_herds:, self.nb_herds:])
        # contribution of infectious pig herds to boars
        # (1, PH).(PH, WB) -> (1, WB)
        self.contribution_pig_to_boar = pwp.dot(self.kernel_boar_static[:self.nb_herds, self.nb_herds:])

        ## compute probabilities of infection for pig herds (sparse matrix)
        ## compute FOI_boar_to_pig + FOI_other_pathways
        trans_b_p = self.get_model_value('transmission_boar_pig')
        trans_o_p = self.get_model_value('transmission_other_pathways')
        ext_risk_reduction = self.get_model_value('external_risk_reduction')
        # reduced external risk when detected or contact
        transmission_other_pathways = np.ones(self.nb_herds) * trans_o_p
        # vigilance = self.detected_pig_herds.maximum(self.protection_pig_herds.maximum(self.surveillance_pig_herds.maximum(self.traced_pig_herds))).nonzero()[1]
        vigilance = (self.detected_pig_herds + self.protection_pig_herds + self.surveillance_pig_herds + self.traced_pig_herds).nonzero()[1]
        # assert(all(vigilance == vig2))
        # transmission_other_pathways[vigilance] *= ext_risk_reduction
        FOI_pigs = (trans_b_p * self.biorisk_outdoor).multiply(self.contribution_boar_to_pig) + (self.biorisk.multiply(transmission_other_pathways)).multiply(self.contribution_other_pathways)
        ## assuming that both boar-to-pig and other pathways are impacted by reinforced biosecurity measures
        # if any(vigilance):
        #     print(FOI_pigs[0, vigilance])
        FOI_pigs[0, vigilance] *= ext_risk_reduction
        # if any(vigilance):
        #     print(FOI_pigs[0, vigilance])
        inf_proba_pigs = 1 - np.exp(- FOI_pigs.todense() * self.model.delta_t)
        infected_pig_herds = np.random.binomial(self.susceptibles, inf_proba_pigs)[0]
        nz = infected_pig_herds.nonzero()
        infected_pig_herds = zip(nz[0] + 1, infected_pig_herds[nz])
        new_infected_herds = []
        if nz[0].any():
            populations = self.get_populations()
            # print('infected pigs, population_id =', list(infected_pig_herds))
            for population_id, new_E in infected_pig_herds:
                # print("infected pigs:", population_id, new_E)
                if population_id in populations:
                    if populations[population_id].statevars._is_active:
                        populations[population_id].expose(new_E)
                else:
                    # print("Creating new infected herds")
                    new_infected_herds.append((population_id, new_E))
        ## compute probabilities of infection for wild boars (sparse matrix)
        ## compute FOI_pig_to_boar + FOI_boar_to_boar
        trans_b_b = self.get_model_value('transmission_boar_boar')
        FOI_wildboars = trans_b_p * self.contribution_pig_to_boar + trans_b_b * self.contribution_boar_to_boar
        inf_proba_wildboars = 1 - np.exp(- FOI_wildboars.todense() * self.model.delta_t)
        # remove natural carcasses and already removed
        inf_proba_wildboars[0, self.boars_natural_carcass.nonzero()] = 0
        inf_proba_wildboars[0, self.boars_infectious_carcass.nonzero()] = 0
        inf_proba_wildboars[0, self.removed_wildboars.nonzero()] = 0
        infected_wildboars = np.random.binomial(1, inf_proba_wildboars)[0].nonzero()[0] + 1 + self.nb_herds
        new_infected_boars = []
        if infected_wildboars.any():
            populations = self.get_populations()
            # print('infected wildboars, population_id =', infected_wildboars)
            for population_id in infected_wildboars:
                if population_id in populations:
                    if populations[population_id].statevars._is_active and populations[population_id].total_S> 0:
                        populations[population_id].expose(1)
                else:
                    new_infected_boars.append(population_id)

        if new_infected_boars or new_infected_herds:
            self.add_new_epidunits(new_infected_herds, new_infected_boars)
        self.reset_status()

    def reset_status(self):
        self.boars_infectious_alive = np.zeros(self.nb_wildboars)
        self.boars_infectious_carcass = np.zeros(self.nb_wildboars)
        self.pig_herd_weighted_prevalence = np.zeros(self.nb_herds)
        self.pig_herd_weighted_prevalence_other_pathways = np.zeros(self.nb_herds)

    def identify_areas(self, pop_id, tile):
        print('Installing protection/surveillance areas for agent', pop_id, 'on tile', tile)
        self.load_neighbours(tile)
        protection_radius = int(self.get_model_value('protection_radius'))
        surveillance_radius = int(self.get_model_value('surveillance_radius'))
        protection_end = self.statevars.step + int(self.get_model_value('protection_duration'))
        surveillance_end = self.statevars.step + int(self.get_model_value('surveillance_duration'))
        # search for herds at distance at most equal to protection_radius
        _, prot_idx, _ = sp.find(self.neighbours[pop_id-1,:] == protection_radius)
        # search for herds at distance at most equal to surveillance_radius
        _, surv_idx, _ = sp.find(self.neighbours[pop_id-1,:] == surveillance_radius)
        print("Protection area", protection_radius, "m around", pop_id, "=>", prot_idx)
        print("Surveillance area", surveillance_radius,"m around", pop_id, "=>", surv_idx)
        # date for exiting protection/surveillance area
        self.protection_end[0, prot_idx] = protection_end
        self.surveillance_end[0, surv_idx] = surveillance_end
        # herds in protection/surveillance area
        self.protection_pig_herds[0, prot_idx] = 1
        self.surveillance_pig_herds[0, surv_idx] = 1
        population = self.get_populations()
        # setup protection/surveillance area
        values = []
        for herd_index in prot_idx:
            values.append([self.statevars.simu_id, self.statevars.step, herd_index+1, 'P', protection_end])
            if herd_index+1 in population:
                population[herd_index+1].statevars.is_in_protection_area = 1
                population[herd_index+1].statevars.date_protection_end = self.protection_end[0, herd_index]
                population[herd_index+1].statevars.changed = True
        ## implement control measure if required:
        ## schedule culling for pig herds in protection area if
        ## control measure already active, but also when protection
        ## areas will still be active at beginning of control measure
        ## => e.g. protection_end > first detection + delay
        date_cull_in_protection_area = self.get_model_value('delay_cull_in_protection_area') + self.statevars.date_first_detection
        if self.get_model_value('cull_in_protection_area') and protection_end >= date_cull_in_protection_area:
            # schedule preventive culling in protection area
            culling_date = int(max(self.statevars.step + self.get_model_value('delay_confirmation') + self.get_model_value('delay_implementation'), date_cull_in_protection_area))
            print("Scheduling preventive culling of herds in protection area: {} at {}".format(prot_idx+1, culling_date))
            if any(prot_idx):
                if culling_date in self.culling_schedule:
                    self.culling_schedule[culling_date] = np.unique(np.concatenate([self.culling_schedule[culling_date], prot_idx]))
                else:
                    self.culling_schedule[culling_date] = prot_idx
                print(self.culling_schedule)
        # setup surveillance area
        for herd_index in surv_idx:
            values.append([self.statevars.simu_id, self.statevars.step, herd_index+1, 'S', surveillance_end])
            if herd_index+1 in population:
                population[herd_index+1].statevars.is_in_surveillance_area = 1
                population[herd_index+1].statevars.date_surveillance_end = self.surveillance_end[0, herd_index]
                population[herd_index+1].statevars.changed = True

        # log pig herds in P/S areas
        if any(values):
            nb_fields = len(values[0])
            message = '\n'.join((','.join(['{}'] * nb_fields)).format(*row) for row in values)
            with open(self.status_log_name, 'a') as logfile:
                logfile.write('{}\n'.format(message))

    def preventive_culling(self):
        """Preventive culling of pig herds 1) within a radius from detected
        carcasses 2) in trade contacts with detected herds 3) in a
        protection area near a detected herd. Each of these measures
        can be implemented or not independently. The process focuses
        in culling all herds already scheduled. Determining whether a
        herd has to be culled depends on each measure when identifying
        the above cases

        """
        if self.statevars.step in self.culling_schedule:
            population = self.get_populations()
            to_cull = self.culling_schedule[self.statevars.step]
            idx_already_culled = []
            cpt = 0

            for idx in to_cull:
              if ((idx+1 in population and population[idx+1].total_epid_unit==0) or (idx+1 not in population and self.susceptibles[0,idx]==0)):
                for step in self.activation_schedule:
                  if step >= self.statevars.step and idx in self.activation_schedule[step]:
                    self.activation_schedule[step].remove(idx)
                date_repop = self.time + int(self.get_model_value('repopulation_delay'))
                if date_repop not in self.activation_schedule:
                  self.activation_schedule[date_repop] = []
                self.activation_schedule[date_repop].append(idx)
              else :
                idx_already_culled.append(cpt)
              cpt += 1

            to_cull=to_cull[idx_already_culled]

            real_herds = [population[idx+1] for idx in to_cull if idx+1 in population]
            virtual_herds = np.array([int(idx) for idx in to_cull if idx+1 not in population], dtype='int32')
            print('Preventive culling at', self.statevars.step, '=>', to_cull)
            print('Real herds to cull:', [h.statevars.population_id for h in real_herds])
            print('Virtual herds to cull:', virtual_herds)
            for herd in real_herds:
                # really remove all animals in simulated herds
                herd.statevars.is_culled = 1 # then let "unit_removal" do the job including logging
                # mark as detected if still infected animals in the herd (in e.g. repopulated herds animals are S)
                herd.statevars.is_detected = int(herd.total_E + herd.total_I + herd.total_C > 0)
                if herd.statevars.is_detected:
                    self.detected_pig_herds[0, herd.index] = 1
                    # identify trade contacts
                    herd.identify_pig_herd_trade_contacts()
                    # and setup protection/surveillance areas
                    self.identify_areas(herd.statevars.population_id, herd.statevars.tile)

            # update information on susceptible herds (real and virtual)
            self.susceptibles[0, to_cull] = 0

            # schedule repopulation of virtual herds only (real are
            # handled by "unit_removal" which can also happen when a
            # detected herd is slaughtered, not only as the result of
            # preventive culling)
            date_repop = self.time + int(self.get_model_value('repopulation_delay'))
            print("Virtual pig herds", virtual_herds, "will be repopulated at t =", date_repop)
            if date_repop not in self.activation_schedule:
                self.activation_schedule[date_repop] = []
            self.activation_schedule[date_repop] += list(virtual_herds)

            # custom log for healthy non-simulated herds
            self.log_preventive_culling(virtual_herds)


    def identify_pig_herds_near_carcass(self, wildboar_carcasses_idx):
        """Search for pig herds within a given radius around wildboar
        carcasses and schedule them for preventive culling.

        """
        by_tile = {}
        target_herds = []
        population = self.get_populations()
        radius = self.get_model_value('radius_cull_near_infected_carcasses')
        print('Searching for pig herds near carcasses:', wildboar_carcasses_idx)
        for wb_idx in wildboar_carcasses_idx:
            wildboar = population[wb_idx + self.nb_herds + 1]
            tile = wildboar.statevars.tile
            if tile not in by_tile:
                by_tile[tile] = []
            by_tile[tile].append((wildboar.statevars.X, wildboar.statevars.Y))
        herds_to_cull_by_tile = {}
        for tile, coords in by_tile.items():
            carcass_coords = np.array(coords)
            col, row = self.coords(tile)
            adjacent_tiles = [(col + i) + (row + j) * self.W
                              for j in [-1, 0, 1]
                              for i in [-1, 0, 1]]
            adjacent_idx = np.concatenate([(self.herds['tile'] == t).to_numpy().nonzero()[0]
                                           for t in adjacent_tiles])
            herds_coord = self.points[adjacent_idx,:]
            d = distance_matrix(herds_coord, carcass_coords)
            local_herds_idx = (d <= radius).nonzero()[0]
            ## TODO: back to real herds index
            herds_to_cull_by_tile[tile] = adjacent_idx[local_herds_idx]

        culling_date = int(self.statevars.step + self.get_model_value('delay_confirmation') + self.get_model_value('delay_implementation'))
        herds_to_cull_idx = np.unique(np.concatenate(list(herds_to_cull_by_tile.values())))
        print("Scheduling preventive culling of herds near infected wildboar carcasses: {} at {}".format(herds_to_cull_idx+1, culling_date))
        if any(herds_to_cull_idx):
            if culling_date in self.culling_schedule:
                self.culling_schedule[culling_date] = np.unique(np.concatenate([self.culling_schedule[culling_date], herds_to_cull_idx]))
            else:
                self.culling_schedule[culling_date] = herds_to_cull_idx


#===============================================================
# CLASS EpidUnit (LEVEL 'epid_unit')
#===============================================================
class EpidUnit(CompartProcessManager):
    """
    Level of the epidemiological unit.
    """

    @property
    def date_first_detection(self):
        """Time when the first detection occurred in the metapopulation,
        either in pig herds or wildboars (0 if no detection yet).

        """
        return self.upper_level().statevars.date_first_detection

    @property
    def index(self):
        """Index of the epidemiological unit in numpy arrays used in the
        python add-on.

        """
        index = int(self.statevars.population_id)-1
        if not self.statevars.is_pig_herd:
            index -= self.upper_level().nb_herds
        return index

    @property
    def is_within_fences(self):
        """1 if the epid unit is within installed fences, 0 otherwise."""
        return self.statevars.tile in self.upper_level().surrounded_tiles

    @property
    def is_within_buffer(self):
        """1 if the epid unit is around installed fences, 0 otherwise."""
        return self.statevars.tile in self.upper_level().surrounding_tiles
    #----------------------------------------------------------------
    # Actions
    #----------------------------------------------------------------
    # def print_hunting_status(self, **others):
    #     # proba_det = self.get_model_value('proba_detection_hunted_wildboars')
    #     # self.statevars.is_tested = random_bool(proba_det)
    #     print(self, 'total_I:', self.total_I, 'total_E:', self.total_E, 'hunted:', self.statevars.is_hunted, 'tested:', self.statevars.is_tested, 'in fences:', self.is_within_fences, 'in buffer:', self.is_within_buffer, 'time:', self.time, 'first:', self.date_first_detection, 'random_bool')
    #     print([self.get_model_value(param)
    #            for param in ['proba_detection_hunted_wildboars',
    #                          'ongoing_hunting_within_fences',
    #                          'ongoing_hunting_within_buffer',
    #                          'hunting_surveillance',
    #                          'is_first_outbreak',
    #                          'delay_confirmation']])
    def remove_carcass(self, **others):
        """Actively remove carcasses when detection occurs through carcasses"""
        ## carcass removal
        if self.statevars.is_detected:
            if self.statevars.date_detected == self.statevars.step:
                to_remove = {'infection': {(self.get_model_value('C'),): self.total_C}}
                self.remove_population(to_remove)
                self.changed = True

    def notify_detection(self, **others):
        """Notify the metapopulation that an outbreak was detected

        """
        if self.statevars.is_detected or (self.statevars.is_tested and self.statevars.is_infected):
            if self.statevars.date_detected == 0:
                    # print("surplus > 0 in", self.statevars.population_id, self.statevars.population, self.total_S, self.total_E, self.total_I, to_remove)
                self.statevars.date_detected = self.statevars.step
                if self.upper_level().statevars.date_first_detection == 0 and self.statevars.is_detected:
                    self.upper_level().statevars.date_first_detection = self.statevars.date_detected
                    print("FIRST DETECTION AT", self.statevars.date_detected)
                # print("\n", self.time, self.statevars.step, self.upper_level().statevars.step, self.statevars.is_detected, self.statevars.date_detected)
                print("DET,{},{},{} {}".format(self.time, self.statevars.population_id, self.statevars.is_pig_herd,
                                               "(tested hunted WB)" if self.statevars.is_hunted and self.statevars.is_tested else ""))

                if self.statevars.is_pig_herd:
                    self.upper_level().detected_pig_herds[0, self.index] = 1
                    self.identify_pig_herd_trade_contacts()
                    self.upper_level().identify_areas(self.statevars.population_id, self.statevars.tile)
                else:
                    ## schedule an active search for WB carcasses
                    ## centered around this case, except for cases
                    ## found within fences/within buffer when increased hunting is
                    ## implemented and active search forbidden in the meanwhile
                    if not ((self.get_model_value('ongoing_hunting_within_fences') and self.is_within_fences and self.get_model_value('forbid_active_search_fences')) or (self.get_model_value('ongoing_hunting_within_buffer') and self.is_within_buffer and self.get_model_value('forbid_active_search_buffer'))):
                        step = int(self.statevars.step + self.get_model_value('delay_carcass_active_search') + self.get_model_value('duration_carcass_active_search'))
                        print("At {}, active search around WB {} scheduled for t={}".format(self.statevars.step, self.statevars.population_id, step))
                        search = self.upper_level().search_schedule
                        if step not in search:
                            search[step] = np.array([], dtype='int32')
                        search[step] = np.concatenate([search[step], [self.statevars.population_id]])
                    ## check if any preventive culling in radius
                    if self.get_model_value('cull_near_infected_carcasses') and self.time >= self.get_model_value('delay_cull_near_infected_carcasses') + self.date_first_detection:
                        self.upper_level().identify_pig_herds_near_carcass([self.index])

                self.statevars.changed = True

    #----------------------------------------------------------------
    # Processes
    #----------------------------------------------------------------

    def update_health_info(self):
        """Update info on health status of this epidemiological unit in the
        metapop.

        """
        index = self.index
        c_E = self.get_model_value('contrib_E_transmission')
        if self.statevars.is_pig_herd:
            self.upper_level().susceptibles[0, index] = self.total_S
            # weighted prevalence is the prevalence multiplied by the
            # biosecurity level and the fact that the herd is outdoor
            if self.total_epid_unit == 0:
                prevalence = 0
            else:
                prevalence = (self.total_E * c_E + self.total_I + self.total_C) / self.total_epid_unit
            # if prevalence < 0:
            #     print(self.statevars.population_id, prevalence, self.total_E, self.total_I, c_E, self.total_S, self.total_C, self.total_D, self.total_epid_unit)
            #     print(self.statevars)
            #     for name, comp in self._content.items():
            #         if name not in self.no_compart:
            #             for subname, subcomp in comp._content.items():
            #                 print(name, subname, subcomp, subcomp.statevars)
            #     sys.exit(-1)
            if self.statevars.is_in_surveillance_area or self.statevars.is_in_protection_area or self.statevars.is_traced or self.statevars.is_detected:
                reinforced_biosecurity = self.get_model_value('external_risk_reduction')
                # print(self.statevars.population_id, 'with reinforced biosecurity', reinforced_biosecurity)
            else:
                reinforced_biosecurity = 1
            self.upper_level().pig_herd_weighted_prevalence[index] = prevalence * (1 - self.statevars.biosecurity) * self.statevars.is_outdoor * reinforced_biosecurity
            # weighted prevalence used for other pathways is the
            # prevalence multiplied by the biosecurity level
            self.upper_level().pig_herd_weighted_prevalence_other_pathways[index] = prevalence * (1 - self.statevars.biosecurity) * reinforced_biosecurity
        else:
            self.upper_level().boars_infectious_alive[index] = self.total_E * c_E + self.total_I
            self.upper_level().boars_infectious_carcass[index] = self.total_C
            self.upper_level().boars_natural_carcass[index] = self.total_D

    def log_custom_vars(self, **others):
        """Store custom information for current epidemiological unit if any
        change occurred.

        """
        ## logged variables:
        ## simu_id, step, population_id, is_infected, is_detected, is_searched, date_detected, is_traced, date_traced_end, is_in_protection_area, date_protection_end, is_in_surveillance_area, date_surveillance_end, is_hunted, is_tested, S, E, I, C, D
        if self.statevars.changed:
            values = [
                self.statevars.simu_id,
                self.statevars.step,
                self.statevars.population_id,
                self.statevars.is_infected,
                self.statevars.is_detected,
                self.statevars.is_searched,
                self.statevars.is_culled,
                self.statevars.date_detected,
                self.statevars.is_traced,
                self.statevars.date_traced_end,
                self.statevars.is_in_protection_area,
                self.statevars.date_protection_end,
                self.statevars.is_in_surveillance_area,
                self.statevars.date_surveillance_end,
                self.statevars.is_hunted,
                self.statevars.is_tested,
                self.total_S,
                self.total_E,
                self.total_I,
                self.total_C,
                self.total_D,
            ]
            message = (','.join(['{}'] * len(values))).format(*values)
            with open(self.log_path(), 'a') as logfile:
                logfile.write('{}\n'.format(message))
            self.statevars.changed = False
            if self.population == 0 and not self.statevars.is_pig_herd:
                ## a wildboar removed is definitively dead (pig herds
                ## could receive new animals through trade)
                self.statevars._is_active = False
                self.upper_level().removed_wildboars[self.index] = 1


    def unit_removal(self):
        """Removal of units where infectious animals were found, possibly
        after a delay. Epidemiological units marked 'is_culled' are
        meant to be removed without delay.

        """
        delay = self.get_model_value('delay_to_removal')
        # remove epidunits 1) when passively detected after a delay 2) when preventive culling is scheduled (is_culled)
        if (self.statevars.is_detected and self.time == self.statevars.date_detected + delay) or self.statevars.is_culled:
            self.statevars._is_active = False
            print("Removing all in", self.statevars.population_id, self.statevars.population)
            self.remove_all()
            # print("Removed all in", self.statevars.population_id, self.statevars.population)
            # if (self.total_epid_unit < 0):
            #     print(self.statevars.population_id, "with population <0 after remove_all")
            #     sys.exit(-1)
            self.statevars.changed = True
            if self.statevars.is_pig_herd:
                date_repop = self.time + int(self.get_model_value('repopulation_delay'))
                print("Pig herd", self.statevars.population_id, "will be repopulated at t =", date_repop)
                repop_sched = self.upper_level().activation_schedule
                if date_repop not in repop_sched:
                    repop_sched[date_repop] = []
                repop_sched[date_repop].append(self.index)
            else:
                index = self.index
                self.upper_level().boars_natural_carcass[index] = 0
                self.upper_level().removed_wildboars[index] = 1

    def identify_pig_herd_trade_contacts(self):
        """If current pig herd is detected, mark is_traced=True in all pig
        herds which have exchanged animals with it within
        pig_contact_window

        """
        window = int(self.get_model_value('pig_contact_window'))
        step = self.statevars.step
        my_id = int(self.statevars.population_id)
        all_moves = self.upper_level().moves
        print('Identifying herds in contact through trade with', my_id, 'between', step - window, 'and', step - 1)
        outgoing_contacts = []
        ingoing_contacts = []
        # stop at step-1 because movements for current step are not yet done and will not be done due to suspicion
        for t in range(step - window, step):
            if t in all_moves:
                for src in all_moves[t]:
                    if src == my_id:
                        # TODO: for data-poor scenarios movements
                        # between backyard pig herds could not be
                        # recorded (hence not taken into account for
                        # contacts)
                        outgoing_contacts += [dest for dest, _ in all_moves[t][my_id]]
                    else:
                        for dest, _ in all_moves[t][src]:
                            if dest == my_id:
                                ingoing_contacts.append(src)
        print('Outgoing contacts:', outgoing_contacts)
        print('Ingoing contacts:', ingoing_contacts)
        suspicious_contacts = np.unique(outgoing_contacts + ingoing_contacts)
        population = self.upper_level().get_populations()
        traced_end = self.upper_level().traced_end
        end_of_suspicion = step + self.get_model_value('trade_suspicion_duration')
        values = []
        for pig_herd_id in suspicious_contacts:
            values.append([self.statevars.simu_id, step, pig_herd_id, 'T', end_of_suspicion])
            self.upper_level().traced_pig_herds[0, pig_herd_id-1] = 1
            traced_end[0, pig_herd_id-1] = end_of_suspicion
            if pig_herd_id in population:
                population[pig_herd_id].statevars.is_traced = 1
                population[pig_herd_id].statevars.date_traced_end = end_of_suspicion
                population[pig_herd_id].statevars.changed = True

        # log suspicious pig herds
        if any(values):
            nb_fields = len(values[0])
            message = '\n'.join((','.join(['{}'] * nb_fields)).format(*row) for row in values)
            with open(self.upper_level().status_log_name, 'a') as logfile:
                logfile.write('{}\n'.format(message))

        ## implement control measure if required:
        ## schedule culling for pig herds in trade contact with
        ## detected case if control measure already active, but also
        ## when suspicion will still be active at beginning of control
        ## measure => e.g. end_of_suspicion > first detection + delay
        date_cull_traced = self.get_model_value('delay_cull_traced_herds') + self.date_first_detection
        if self.get_model_value('cull_traced_herds') and end_of_suspicion >= date_cull_traced:
            # schedule preventive culling for traced herds
            culling_date = int(max(self.statevars.step + self.get_model_value('delay_confirmation') + self.get_model_value('delay_implementation'), date_cull_traced))
            print("Scheduling preventive culling of traced herds {} at {}".format(suspicious_contacts, culling_date))
            culling_schedule = self.upper_level().culling_schedule
            if any(suspicious_contacts):
                if culling_date in culling_schedule:
                    culling_schedule[culling_date] = np.unique(np.concatenate([culling_schedule[culling_date], suspicious_contacts-1]))
                else:
                    culling_schedule[culling_date] = suspicious_contacts-1 # population_ids -> indices
                print(culling_schedule[culling_date])

    def integrate_purchased_pigs(self):
        """Check incoming hosts (put in 'inbox') and add them to local
        population. If sales at the end of previous time step or
        purchased at the beginning of the current time step lead to a
        population size that does not correspond to the initial herd
        size, either new S animals are purchased, or S/E/I animals are
        sold randomly (also compensating for carcasses).

        """
        # collect incoming hosts from 'inbox'
        if self.statevars.is_pig_herd and not (self.statevars.is_detected or self.statevars.is_traced or self.statevars.is_in_protection_area or self.statevars.is_in_surveillance_area):
            purchased = {(self.get_model_value(state),): 0
                         for state in ['S', 'E', 'I']}
            for message in self._inbox:
                if 'hosts' in message:
                    # print(message)
                    for state, qty in message['hosts'].items():
                        purchased[(self.get_model_value(state),)] += qty

            total_purchased = sum(purchased.values()) if purchased else 0
            if total_purchased > 0:
                self.statevars.changed = True
            self.statevars.purchased += total_purchased
            nb_S, nb_E, nb_I = self.total_S, self.total_E, self.total_I
            total_living = nb_S + nb_E + nb_I
            surplus = total_living + total_purchased - self.statevars.size
            if surplus > 0:
                ## the final amount of animals in the herd will be too
                ## high (compared to expected size), which means that
                ## current living animals in the herd must be removed
                ## (to slaughterhouse for instance).
                if total_living > 0:
                    # this indeed cannot be done if no more animals in herd
                    S = (self.get_model_value('S'),)
                    E = (self.get_model_value('E'),)
                    I = (self.get_model_value('I'),)
                    qty_by_state = np.random.multinomial(surplus, np.array([nb_S, nb_E, nb_I])/total_living)
                    # remove as many animals as required to maintain
                    # constant herd size, states being chosen randomly
                    # except carcasses
                    to_remove = {'infection':
                                 {S: qty_by_state[0],
                                  E: qty_by_state[1],
                                  I: qty_by_state[2]
                                 }}
                    # print("surplus > 0 in", self.statevars.population_id, self.statevars.population, self.total_S, self.total_E, self.total_I, to_remove)
                    self.remove_population(to_remove)
            elif surplus < 0:
                purchased[(self.get_model_value('S'),)] += -surplus
            # add newly introduced animals
            self.add_population({'infection': purchased})
            self.clean_inbox()

    def expose(self, nb):
        new_E = min(self.total_S, nb)
        if new_E > 0:
            # print("S->E in", self.statevars.population_id, self.total_S, nb, new_E)
            self.remove_population({'infection': {(self.get_model_value('S'),): new_E}})
            self.add_population({'infection': {(self.get_model_value('E'),): new_E}})
            self.statevars._is_active = self.total_epid_unit > 0
            self.statevars.is_infected = 1
            self.statevars.changed = True
            # self.log_custom_vars()

    def perform_sales(self):
        """Select hosts to be sold and move them to 'outbox'.

        """
        if self.statevars.is_pig_herd and self.statevars._is_active and not (self.statevars.is_detected or self.statevars.is_traced or self.statevars.is_in_protection_area or self.statevars.is_in_surveillance_area):
            # clean outgoing messages
            self.reset_outbox()
            # check if something to do at current time step for this population
            step = self.statevars.step
            my_id = int(self.statevars.population_id)
            all_moves = self.upper_level().moves
            self.statevars.sold = 0
            if step in all_moves:
                if my_id in all_moves[step]:
                    my_moves = all_moves[step][my_id]
                    for dest, qty in my_moves:
                        if self.upper_level().detected_pig_herds[0, dest-1] or self.upper_level().protection_pig_herds[0, dest-1] or self.upper_level().surveillance_pig_herds[0, dest-1] or self.upper_level().traced_pig_herds[0, dest-1]:
                            continue # skip sale
                        # find convenient animals
                        S = (self.get_model_value('S'),)
                        E = (self.get_model_value('E'),)
                        I = (self.get_model_value('I'),)
                        nb_S_src, nb_E_src, nb_I_src = self.total_S, self.total_E, self.total_I
                        N_src = nb_S_src + nb_E_src + nb_I_src
                        qty_to_sell = min(N_src, qty)
                        if qty_to_sell == 0:
                            continue
                        qty_by_state = np.random.multinomial(qty_to_sell, np.array([nb_S_src, nb_E_src, nb_I_src])/N_src)
                        # print(qty)
                        # create message corresponding to the
                        # population sold messages use population_id
                        # (which start at 0) rather than herds id as
                        # in moves.csv and herds.csv
                        message = {'src': my_id, 'dest': dest,
                                   'hosts': {
                                       'S': qty_by_state[0],
                                       'E': qty_by_state[1],
                                       'I': qty_by_state[2]
                                   }
                        }
                        print(self.statevars.step, message)
                        self.add_outbox(message)
                        # remove population sold. Replacement by S
                        # animals if size too low done after checking
                        # the balance with potentially purchased
                        # animals
                        to_remove = {'infection':
                                     {S: qty_by_state[0],
                                      E: qty_by_state[1],
                                      I: qty_by_state[2]
                                     }}
                        # print("Sales in", self.statevars.population_id, self.statevars.population, self.total_S, self.total_E, self.total_I, to_remove)
                        self.remove_population(to_remove)
                        self.statevars.sold += qty_to_sell
                    if self.statevars.sold > 0:
                        self.statevars.changed = True
                    # if self.total_epid_unit < 0:
                    #     print("Total epid unit < 0 after sales:", self.statevars.population_id)
                    #     sys.exit(-1)
